#!/bin/bash
source util.sh

cd $group/$(head -1 $group.list)
git pull >/dev/null
cd ..

current=$(grep "^pkgver=" plasma-mobile/PKGBUILD | cut -d= -f2)

msg "Current version is: $current"
msg2 "New version: "
read new
msg "update PKGBUILDs [$current] > [$new]? [Y/n]"
read choice
if [[ "$choice" = "n" ]]; then
    exit 1
fi

for p in $(sed -e '/^#/d' ../$group.list); do
    msg "updating $p"
    cd $p
    git pull
    sed -i "/^pkgver=/c\pkgver=$new" PKGBUILD
    clean
    updpkgsums
    clean
    git add PKGBUILD
    git commit -m"ver $new"
    git push
    cd ..
done
