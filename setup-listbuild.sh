#!/bin/bash
source util.sh

# collect credentials
msg2 "gitlab-user: "
read user
printf "password: "
read -s password
echo ""

_credentials=$user:"$password"@
_git=https://${_credentials}gitlab.manjaro.org/manjaro-arm/packages/extra/plasma-mobile-additional

[[ ! -d $group ]] && mkdir $group
cd $group
for p in $(cat ../$group.list); do
    [[ -d $p ]] && msg "$p already exists; skipping" || git clone $_git/$p.git
done
cd ..

msg "Ready."
