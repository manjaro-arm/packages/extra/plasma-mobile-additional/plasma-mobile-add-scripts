1. **setup-listbuild.sh** - collects gitlab credentials and checks out plasma versioned package repos (plasma-mobile-settings and plasma-mobile-sounds excluded)

2. **bump-version.sh** - displays current repo version of the package group (according to the host's branch) and asks for the desired new version, then updates all PKGBUILDs and commits the changes to gitlab

4. **build-list.sh** - builds the packages in 'plasma-mobile.list' with **manjaro-chrootbuild**
